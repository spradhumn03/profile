const stylingskills = [
    {
      name: "HTML",
      percent: "95%"
    },
    {
        name: "CSS",
        percent: "80%"
    },
    {
        name: "SASS",
        percent: "90%"
    },
    {
        name: "LESS",
        percent: "80%"
    }
  ];
  const behaviourialskills = [
    {
      name: "JAVASCRIPT",
      percent: "75%"
    },
    {
        name: "JQUERY",
        percent: "70%"
    },
    {
        name: "REACT",
        percent: "50%"
    },
    {
        name: "BOOTSTRAP",
        percent: "90%"
    }
  ];
export default stylingskills;
export {behaviourialskills};