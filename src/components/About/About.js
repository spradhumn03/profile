import React from 'react';
import skills, {behaviourialskills} from './aboutskills';
// stylingskills
// // const stylingskills = props;
    
const About = (props) => {
    const styleingskills = skills.map((skill)=>{
        return(
                <div key={skill.name}>
                    <p>{skill.name}</p>
                    <div className="w3-round-xlarge" style={{ background:'#e4e1e1' }}>
                        <div className="w3-container w3-blue w3-padding w3-center w3-round-xlarge" style={{width:skill.percent}}>{skill.percent}</div>
                    </div>
                </div>
        );
      });
      const behaviourialskill = behaviourialskills.map((skill)=>{
        return(
                <div key={skill.name}>
                    <p>{skill.name}</p>
                    <div className="w3-round-xlarge" style={{ background:'#e4e1e1' }}>
                        <div className="w3-container w3-blue w3-padding w3-center w3-round-xlarge" style={{width:skill.percent}}>{skill.percent}</div>
                    </div>
                </div>
        );
      });
    return(
            <div className="w3-container w3-padding-large" style={{marginBottom:"32px"}}>
                <h2><b>About Me</b></h2>
                <p>I am a Professional Web and desktop app Developer. And an expert in React, Node, Firebase, JavaScript, ES6+, CSS. I worked for one year as Shopify developer, which is an e-commerce platform. Working with an IT firm named Startbit IT Solutions Pvt. Ltd. </p>

                <p>I am comfortable rolling out my own CSS whenever or working with Bootstrap, Semantic, SASS or LESS. I am familiar with concepts of React Native also. I have worked on integrating Firebase with my React project to access the public Apis created in Ruby on Rails. I also know about authentication management in firebase.</p>

                <p>I am a fan of online courses and like to actively contributing my knowledge and skills about the coding. I keep training myself with various courses in my freetime. Currently I only contribute to Stackoverflow due to tight schedule, But soon I will start contributing on github also.</p>
                <hr/>
        
                <h4>Technical Skills</h4>
                <div className="w3-row">
                    <div className="w3-col m6 w3-padding l6 s12">
                        {styleingskills}       
                    </div>
                    <div className="w3-col w3-padding m6 l6 s12">
                        {behaviourialskill}
                    </div>
                </div>
                
                <a href="/Data.txt" download="Pradhumn_Resume" style={{textDecoration:'none'}}>
                    <button className="w3-btn w3-black w3-padding-large w3-margin-top w3-margin-bottom">
                        <i className="fa fa-download w3-margin-right"></i><b>Download Resume</b>
                    </button>
                </a>
            </div>
        );
}

export default About;