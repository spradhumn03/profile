import React from 'react';
import {Link} from 'react-router-dom';
import './Navigation.css';
function w3_open() {
        document.getElementById("mySidebar").style.display = "block";
        document.getElementById("myOverlay").style.display = "block";
        document.getElementById("mySidebar").classList.remove("animate-close");
}
 
function w3_close(e) {

        document.getElementById("mySidebar").classList.add("animate-close");
        setTimeout(function(){document.getElementById("mySidebar").style.display = "none";}, 400);
        document.getElementById("myOverlay").style.display = "none"; 
        // document.getElementById("mySidebar").classList.add("animate-close");
        // setTimeout(function(){document.getElementById("mySidebar").style.display = "none";}, 400);
        // document.getElementById("myOverlay").style.display = "none"; 
}

const Navigation = (props) =>{
    return(
        <div>
        <nav className="w3-sidebar w3-collapse w3-white w3-animate-left" style={{zIndex:'3', width:'300px'}} id="mySidebar"><br/>
            <div className="w3-container">
                <span href="#" onClick={w3_close} className="w3-hide-large w3-right w3-jumbo w3-padding w3-hover-grey" title="close menu">
                <i className="fa fa-remove"></i>
                </span>
                <img src="../images/avatar_g2.png" alt="Profile pic" style={{width:'45%'}} className=""/><br/><br/>
                <h4><b>Pradhumn Sharma</b></h4>
                <p className="w3-text-grey">Shopify and React developer</p>
            </div>
            <div className="w3-bar-block">
                <Link to="/" onClick={props.width < 992 ? w3_close: null} className="w3-bar-item w3-button w3-padding"><i className="fa fa-th-large fa-fw w3-margin-right"></i>HOME</Link> 
                <Link to="/about" onClick={props.width < 992 ? w3_close: null} className="w3-bar-item w3-button w3-padding"><i className="fa fa-user fa-fw w3-margin-right"></i>ABOUT</Link> 
                <Link href="/portfolio" to="/portfolio" onClick={props.width < 992 ? w3_close: null} className="w3-bar-item w3-button w3-padding"><i className="fa fa-user fa-fw w3-margin-right"></i>PORTFOLIO</Link> 
                {/* <Link to="/skills-education" onClick={props.width < 992 ? w3_close: null} className="w3-bar-item w3-button w3-padding"><i className="fa fa-user fa-fw w3-margin-right"></i>EDUCATION</Link>  */}
                <Link to="/contact" onClick={props.width < 992 ? w3_close: null} className="w3-bar-item w3-button w3-padding"><i className="fa fa-envelope fa-fw w3-margin-right"></i>CONTACT</Link>
            </div>
            <div className="w3-panel w3-large">
                <a target="_blank" rel="noopener noreferrer" href="https://bitbucket.org/spradhumn03/profile"><i className="fa fa-github w3-hover-opacity header-icons"></i></a>
                <a target="_blank" rel="noopener noreferrer" href="https://stackoverflow.com/users/9724062/pradhumn-sharma"><i className="fa fa-stack-overflow w3-hover-opacity header-icons"></i></a>
                <a href="/" rel="noopener noreferrer"><i className="fa fa-linkedin w3-hover-opacity header-icons"></i></a>
            </div>
        </nav>
        <div className="w3-overlay w3-hide-large w3-animate-opacity" onClick={w3_close} style={{cursor:'pointer'}} title="close side menu" id="myOverlay"></div>
        <div className="w3-main" style={{marginLeft:'300px'}}>
          
            <header id="portfolio">
                <a href="/"><img src="../images/avatar_g3.png" alt="portfolio" style={{width:'65px'}} className="w3-right w3-margin w3-hide-large w3-hover-opacity"/></a>
                <span className="w3-button w3-hide-large w3-xxlarge w3-hover-text-grey" onClick={w3_open}><i className="fa fa-bars"></i></span>
            </header>
                 
        </div>
        
        </div>
    );
}

export default Navigation;
export {w3_open, w3_close};