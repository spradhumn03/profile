import React, { Component } from 'react';
// import logo from './logo.svg';
// import './App.css';
import SwitchWithSlide from "./SwitchwithSlider";
import Navigation from './components/Navigation/Navigation';
import {BrowserRouter as Router, Route} from  'react-router-dom';
import Home from './components/Home/Home';
import About from './components/About/About';
import Portfolio from './components/Portfolio/Portfolio';
import Contact from './components/Contact/Contact';

class App extends Component {
  constructor(){
    super();
    this.state={
      width:0,
      height:0
    };
    this.updateWindowDimensions=this.updateWindowDimensions.bind(this);
  }
  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }
  
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }
  
  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <Navigation width={this.state.width}/>
            <div className="w3-main w3-container" style={{marginLeft:"300px"}}>
              <SwitchWithSlide>
                <Route exact path="/" component={Home} />
                <Route path="/about" component={About} />
                <Route path="/portfolio" component={Portfolio} />
                <Route path="/contact" component={Contact} />
              </SwitchWithSlide>
            </div>
          </div>    
        </Router>
      </div>
    );
  }
}

export default App;
